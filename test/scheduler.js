var expect = require('chai').expect;
var job = require('../src/job/job');
var sinon = require('sinon');
var scheduler = require('../src/scheduler/scheduler');

describe('Scheduler', function () {
    var clock;
    var mock, mock2;

    beforeEach(function () {
        clock = sinon.useFakeTimers();
        mock = {
            type: 'logger',
            when: {
                date: 'now',
                repeat: {
                    times: 6,
                    interval: 1000
                }
            },
            args: {
                text: 'log text'
            }
        };
        mock2 = {
            type: 'logger',
            when: {
                date: 'in 2 seconds',
                repeat: {
                    times: 6,
                    interval: 'every 3 seconds'
                }
            },
            args: {
                text: 'log text'
            }
        }
    });

    afterEach(function () {
        clock.restore();
    });

    it('should pass', function () {
        expect(true).to.be.true;
    });

    it('should run once and done', function () {
        delete mock.when.repeat;
        var j = job.create(mock).job;
        scheduler.schedule(j);
        clock.tick(500);
        expect(j.done).to.be.true;
        expect(j.invocations).to.equal(1);
    });

    it('should run once and not done', function () {
        var j = job.create(mock).job;
        scheduler.schedule(j);
        clock.tick(500);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(1);
    });

    it('should run "repeat.times" times', function () {
        var j = job.create(mock).job;
        scheduler.schedule(j);
        clock.tick(6100);
        expect(j.done).to.be.true;
        expect(j.invocations).to.equal(j.repeatTimes);
    });

    it('should stop run after unschedule', function () {
        var j = job.create(mock).job;
        scheduler.schedule(j);
        clock.tick(2100);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(3);

        scheduler.unschedule(j.id);
        clock.tick(9100);
        expect(j.done).to.be.true;
        expect(j.invocations).to.equal(3);
    });

    it('should run as the spec says', function () {
        var j = job.create(mock2).job;
        scheduler.schedule(j);
        clock.tick(1000);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(0);
        clock.tick(1100);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(1);
        clock.tick(4000);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(2);
        clock.tick(10000);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(5);
        clock.tick(3000);
        expect(j.done).to.be.true;
        expect(j.invocations).to.equal(6);
    });

    it('should run forever', function () {
        delete mock.when.repeat.times;
        var j = job.create(mock).job;
        scheduler.schedule(j);
        clock.tick(100);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(1);
        clock.tick(1000);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(2);
        for (var i = 3; i < 10; i++) {
            clock.tick(1000);
            expect(j.done).to.be.false;
            expect(j.invocations).to.equal(i);
        }
        scheduler.unschedule(j.id);
        expect(j.done).to.be.true;
    })

    it('should update correctly 1', function () {
        var j = job.create(mock).job;
        scheduler.schedule(j);
        clock.tick(100);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(1);
        clock.tick(1000);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(2);
        scheduler.update({
            id: j.id,
            when: {
                date: 'in 3 seconds',
                repeat: {
                    times: 3,
                    interval: 'every 5 second'
                }
            }
        });
        clock.tick(1100);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(0);
        clock.tick(2000);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(0);
        clock.tick(1000);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(1);
        clock.tick(15000);
        expect(j.done).to.be.true;
        expect(j.invocations).to.equal(3);
    });

    it('should update correctly 2', function () {
        var j = job.create(mock).job;
        scheduler.schedule(j);
        clock.tick(100);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(1);
        clock.tick(1000);
        expect(j.done).to.be.false;
        expect(j.invocations).to.equal(2);
        scheduler.update({
            id: j.id,
            when: {
                date: 'now'
            }
        });
        clock.tick(100);
        expect(j.done).to.be.true;
        expect(j.invocations).to.equal(1);
    });

    it('should not be able to update job after done', function () {
        delete mock.when.repeat;
        var j = job.create(mock).job;
        scheduler.schedule(j);
        clock.tick(500);
        expect(j.done).to.be.true;
        expect(j.invocations).to.equal(1);
        expect(scheduler.update(scheduler.update({
            id: j.id,
            when: {
                date: 'in 3 seconds',
                repeat: {
                    times: 3,
                    interval: 'every 5 second'
                }
            }
        }))).to.be.false;
    })
});