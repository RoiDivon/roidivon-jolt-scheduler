var expect = require('chai').expect;
var job = require('../src/job/job');

describe('Job', function() {

    var mock;
    beforeEach(function () {
        mock = {
            type: 'logger',
            when: {
                date: 'now',
                repeat: {
                    times: 6,
                    interval: 1000
                }
            },
            args: {
                text: 'log text'
            }
        }
    });

    it('should fail if json is null', function() {
        var parsed = job.create(null);
        expect(parsed.job).to.not.exist;
        expect(parsed.error).to.exist;
        expect(parsed.error).to.equal('must provide body');
    });

    it('should fail if json is empty', function() {
        var parsed = job.create({});
        expect(parsed.job).to.not.exist;
        expect(parsed.error).to.exist;
        expect(parsed.error).to.equal('must provide body');
    });

    it('should fail if no type is given', function() {
        delete mock.type;
        var parsed = job.create(mock);
        expect(parsed.job).to.not.exist;
        expect(parsed.error).to.exist;
        expect(parsed.error).to.equal('must provide type');
    });

    it('should fail if no args are given', function() {
        delete mock.args;
        var parsed = job.create(mock);
        expect(parsed.job).to.not.exist;
        expect(parsed.error).to.exist;
        expect(parsed.error).to.equal('must provide args');
    });

    it('should fail if no when is given', function() {
        delete mock.when;
        var parsed = job.create(mock);
        expect(parsed.job).to.not.exist;
        expect(parsed.error).to.exist;
        expect(parsed.error).to.equal('must provide when');
    });

    it('should fail if no start date is given', function() {
        delete mock.when.date;
        var parsed = job.create(mock);
        expect(parsed.job).to.not.exist;
        expect(parsed.error).to.exist;
        expect(parsed.error).to.equal('must provide start date');
    });

    it('should fail if start date is not valid 1', function() {
        mock.when.date = 'sdfsdf';
        var parsed = job.create(mock);
        expect(parsed.job).to.not.exist;
        expect(parsed.error).to.exist;
        expect(parsed.error).to.equal('invalid start time');
    });

    it('should fail if start date is not valid 2', function() {
        timeInPast = (new Date()).getTime() - 1000;
        mock.when.date = timeInPast;
        var parsed = job.create(mock);
        expect(parsed.job).to.not.exist;
        expect(parsed.error).to.exist;
        expect(parsed.error).to.equal('invalid start time');
    });

    it('should fail if set repeat with no interval', function() {
        delete mock.when.repeat.interval;
        var parsed = job.create(mock);
        expect(parsed.job).to.not.exist;
        expect(parsed.error).to.exist;
        expect(parsed.error).to.equal('invalid repeat interval');
    });

    it('should creat job with now', function () {
        var parsed = job.create(mock);
        expect(parsed.error).to.not.exist;
        expect(parsed.job).to.exist;
    });

    it('should creat job with time in future', function () {
        timeInFuture = (new Date()).getTime() + 1000;
        mock.when.date = timeInFuture;
        var parsed = job.create(mock);
        expect(parsed.error).to.not.exist;
        expect(parsed.job).to.exist;
    });

});