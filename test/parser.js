var expect = require('chai').expect;
var parser = require('../src/parser/parser');

describe('Parser', function () {

    it('should return null with null input', function () {
        expect(parser.parseRepeatInterval(null)).to.be.null;
        expect(parser.parseStartTime(null)).to.be.null;
    });

    it('should return null with invalid input', function () {
        expect(parser.parseStartTime('at 11 days')).to.be.null;
        expect(parser.parseStartTime('in sdf days')).to.be.null;
        expect(parser.parseStartTime('in 11 dayss')).to.be.null;

        expect(parser.parseRepeatInterval('everyyy 11 days')).to.be.null;
        expect(parser.parseRepeatInterval('every roi days')).to.be.null;
        expect(parser.parseRepeatInterval('every 11 hourr')).to.be.null;
    });

    it('should return 1 for now as start time', function () {
        expect(parser.parseStartTime('now')).to.equal(1);
    });

    it('should return null for 0 in repeat time', function () {
        expect(parser.parseRepeatInterval('every 0 minutes')).to.be.null;
    });

    it('should return null for start time in the past', function () {
        expect(parser.parseStartTime(Date.now() - 100)).to.be.null;
        expect(parser.parseStartTime('in -22 days')).to.be.null;
    });

    it('should return null for negative or 0 repeat interval', function () {
        expect(parser.parseRepeatInterval(-100)).to.be.null;
        expect(parser.parseRepeatInterval(0)).to.be.null;
        expect(parser.parseRepeatInterval('every -22 days')).to.be.null;
    });

    it('should return time period in long', function () {
        expect(parser.convertToLong('every 5 minutes')).to.equal(300000);
        expect(parser.convertToLong('every 3 hours')).to.equal(10800000);
        expect(parser.convertToLong('every 15 seconds')).to.equal(15000);
        expect(parser.convertToLong('every 2 days')).to.equal(172800000);
    });


});