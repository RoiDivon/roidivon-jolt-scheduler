var scheduler = require('../../src/scheduler/scheduler');
var express = require('express');
var router = express.Router();
var job = require('../../src/job/job');

router.post('/', function(req, res) {
    const parsed = job.create(req.body);
    if (parsed.error) {
        res.send({ error: parsed.error });
        return;
    }
    if (scheduler.schedule(parsed.job)) {
        res.send({id: parsed.job.id});
    } else {
        res.send({error: 'failed to schedule job'});
    }
});

router.delete('/', function(req, res) {
    if (scheduler.unschedule(req.body.id)) {
        res.sendStatus(204);
    } else {
        res.send({error: 'invalid job id'});
    }

});

router.put('/', function (req, res) {
    if (scheduler.update(req.body)) {
        res.sendStatus(204);
    } else {
        res.send({error: 'invalid job id or params'});
    }

});

module.exports = router;
