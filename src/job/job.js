var uuid = require('uuid');
var _ = require('lodash');
var parser = require('../parser/parser');

function Job(jobInfo, uid) {

    this.id = uid ? uid : uuid.v1();
    this.type = jobInfo.type;
    this.args = jobInfo.args;
    this.startOffset = parser.parseStartTime(jobInfo.when.date) ;
    if (jobInfo.when.repeat) {
        this.repeatTimes = jobInfo.when.repeat.times;
        this.repeatInterval = parser.parseRepeatInterval(jobInfo.when.repeat.interval);
    }
    this.timeout = null;
    this.invocations = 0;
    this.done = false;
}

function create (json) {
    var result = { job: null, error: null }
    if (!json || _.isEmpty(json)) {
        result.error = 'must provide body';
    } else if (!json.type) {
        result.error = 'must provide type';
    } else if (!json.args) {
        result.error = 'must provide args';
    } else if (!json.when) {
        result.error = 'must provide when';
    } else if (!json.when.date) {
        result.error = 'must provide start date';
    } else if (!parser.parseStartTime(json.when.date)) {
        result.error = 'invalid start time';
    } else if (json.when.repeat && !parser.parseRepeatInterval(json.when.repeat.interval)) {
        result.error = 'invalid repeat interval';
    } else {
        result.job = new Job(json);
    }
    return result;
};

exports.create = create;
