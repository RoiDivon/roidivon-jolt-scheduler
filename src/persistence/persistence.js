var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient

const MONGO_URL = 'mongodb://localhost/jolt';

function Persistence(onConnect) {
    var self = this;

    this.db = null;
    MongoClient.connect(MONGO_URL, function(err, db) {
        if (err) {
            onConnect(err);
        }  else {
            self.db = db;
            console.log("Connected correctly to server");
            onConnect(null);
        }
    });
}

Persistence.prototype.getAllJobs = function (cb) {
    console.log('getAllJobs');
    var collection = this.db.collection('jobs');
    collection.find({}).toArray(function (err, result) {
        console.log({result: result});
        onDone(err, result, cb);
    })
};

Persistence.prototype.addJob = function (jobInfo, cb) {
    var collection = this.db.collection('jobs');
    collection.insertOne(jobInfo, function(err, result) {
        onDone(err, result.ops[0]._id, cb);
    });
};

Persistence.prototype.removeJob = function (uid, cb) {
    var collection = this.db.collection('jobs');
    collection.deleteOne({uid: uid}, function (err, result) {
        onDone(err, true, cb);
    })
};

Persistence.prototype.decRepeatTimes = function (job) {
    var collection = this.db.collection('jobs');
    var when = job.when;
    when.repeat.times = when.repeat.times - 1;
    collection.updateOne({uid: job.uid}, { $set: { when: when } },  function (err, result) {
        onDone(err, true);
    });
}

function onDone (err, data, cb) {
    if (!cb) {
        return;
    }
    if (err) {
        cb(err, null);
    } else {
        cb(null, data);
    }
}

exports.Persistence = Persistence;
