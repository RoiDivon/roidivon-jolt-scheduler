var request = require('request');

function HttpExecutor () {
}

HttpExecutor.prototype.execute = function (args) {
    const url = args.url;
    const method = args.method.toString().toUpperCase();
    const body = args.body;
    const headers = args.headers;

    request({
        url: url,
        method: method,
        json: true,
        body: body,
        headers: headers
    }, function (err, res, body) {
        if (err) {
            console.log('http failed with ' + err);
        }
    });
};

exports.HttpExecutor = HttpExecutor;