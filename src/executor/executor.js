var ExecutorFactory = require('./factory').ExecutorFactory;

function Executor () {

    this.executors = {};
    this.factory = new ExecutorFactory();
}

Executor.prototype.execute = function (job, onPostExecute) {
    const type = job.type;
    var executor = this.executors[type];
    if (!executor) {
        this.executors[type] = this.factory.get(type);
        executor = this.executors[type];
    }
    if (!executor) {
        return;
    }
    executor.execute(job.args);
    onPostExecute(job);
};

exports.Executor = Executor;