var HttpExecutor = require('./types/httpExecutor').HttpExecutor;
var LoggerExecutor = require('./types/loggerExecutor').LoggerExecutor;
var SmsExecutor = require('./types/smsExecutor').SmsExecutor;

function ExecutorFactory () {

}

ExecutorFactory.prototype.get = function (type) {
    switch (type) {
        case 'http':
            return new HttpExecutor();
        case 'logger':
            return new LoggerExecutor();
        case 'sms':
            return new SmsExecutor();
        default:
            return null;
    }
};

exports.ExecutorFactory = ExecutorFactory;