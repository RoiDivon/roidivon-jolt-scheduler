function parseStartTime (source) {
    if (!source) {
        return null;
    }
    var now = Date.now();
    if (typeof source === 'number') {
        var time = parseInt(source);
        return now < time ? time : null;
    }
    if (source === 'now') {
        return 1;
    }
    if (!source.match(/^in\s(\d+)\s(minute|minutes|second|seconds|hour|hours|day|days)$/)) {
        return null;
    }
    return now + convertToLong(source);
}

function parseRepeatInterval (source) {
    if (!source) {
        return null;
    }
    if (typeof source === 'number') {

        var time = parseInt(source);
        return time > 0 ? time : null;
    }
    if (!source.match(/^every\s(\d+)\s(minute|minutes|second|seconds|hour|hours|day|days)$/)) {
        return null;
    }
    var interval = convertToLong(source);
    return interval == 0 ? null : interval;
}

function convertToLong (source) {
    var tokens = source.split(' ');
    return parseInt(tokens[1]) * convertTimeUnitToLong(tokens[2]);
}

function convertTimeUnitToLong(unit) {
    switch (unit) {
        case 'second':
        case 'seconds':
            return 1000;
        case 'minute':
        case 'minutes':
            return 60000;
            break;
        case 'hour':
        case 'hours':
            return 3600000;
            break;
        case 'day':
        case 'days':
            return 86400000;
    }
}

exports.parseStartTime = parseStartTime;
exports.parseRepeatInterval = parseRepeatInterval;
exports.convertToLong = convertToLong;