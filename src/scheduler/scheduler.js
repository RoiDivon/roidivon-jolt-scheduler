var Executor = require('../executor/executor').Executor;
var parser = require('../parser/parser');
var _ = require('lodash');

var jobs = [];
var executor = new Executor();

function Scheduler() {
}

function schedule(job) {
    jobs[job.id] = job;
    job.timeout = setTimeout(function () {
        executeJob(job);
    }, job.startOffset);
    return true;
}

function unschedule(jobId) {
    var job = jobs[jobId];
    if (!job) {
        return false;
    }
    if (job.timeout) {
        clearTimeout(job.timeout);
        job.timeout = null;
    }
    job.done = true;
    delete jobs[job.id];
    return true;
}

function update(args) {
    var id = args.id;
    var when = args.when;
    var job = jobs[id];
    if (!job) {
        return false;
    }
    var startTime = parser.parseStartTime(when.date);
    if (!startTime) {
        return false;
    }
    if (when.repeat) {
        var interval = parser.parseRepeatInterval(when.repeat.interval);
        if (!interval) {
            return false;
        }
    } else {
        job.repeatInterval = undefined;
        job.repeatTimes = undefined;
    }
    if (job.timeout) {
        clearTimeout(job.timeout);
    }
    job.startOffset = startTime;
    if (when.repeat) {
        job.repeatTimes = when.repeat.times;
        job.repeatInterval = interval;
    }
    job.invocations = 0;
    return schedule(job);
}

function executeJob(job) {
    job.invocations++;
    executor.execute(job, onPostExecute);
}

function onPostExecute(job) {
    if (!job.repeatInterval || job.repeatTimes == job.invocations) {
        unschedule(job.id);
        return;
    }
    job.timeout = setTimeout(function () {
        executeJob(job);
    }, job.repeatInterval);
}

exports.schedule = schedule;
exports.unschedule = unschedule;
exports.update = update;
